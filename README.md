# NaWece

Webpage for students of Faculty of Electronics. Written in Django and that's why it's so pretty. 

## Start-up

You must have Python 3 installed.

Install virtualenv:
> pip install virtualenv

For Debian-based Linux distros:
> sudo apt-get install python3-venv

Initialize and run the Python's virtual environment:
> python3 -m venv env
> . env/bin/activate 

Install the necessary dependencies.
> pip install -r requirements.txt

If everything goes smoothly, run the test server:
> python manage.py runserver
